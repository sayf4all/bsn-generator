package nl.multicode.bsngenerator;

import java.util.Set;


public class App {

    public static void main(String[] args) {
        Integer aantal = getAantal(args);
        BsnGenerator bsnGenerator = new BsnGenerator();

        Set<String> bsnSet = bsnGenerator.generateRandomBsnNummers(aantal);

        for (String bsn : bsnSet) {
            System.out.println(bsn);
        }
    }

    private static Integer getAantal(String[] args) {
        if(args!=null && args.length>0){
            return Integer.valueOf(args[0]);
        }
        return 1;
    }
}
